package com.demo.crosswords.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.demo.crosswords.BaseScreen;
import com.demo.crosswords.BlockGame;
import com.demo.crosswords.ScreenManager;

import java.util.Random;

public class MenuLevelScreen extends BaseScreen{

	private TextureAtlas atlas;
	private Skin skin;
	private TextButton Levels[] , btn_back;
	private TextButtonStyle Lever_Style[];
	private Sound sound_Menu;
	private int col , row , offset_x , offset_y , gap , size , height , width;
	
	@Override
	public void show() {
		super.show();
		height = BaseScreen.VIEWPORT_HEIGHT;
		width = BaseScreen.VIEWPORT_WIDTH;
		new SpriteBatch();
		CreateDB();
		LoadImage();
		sound_Menu = BlockGame.sound_Stage;
	}
	
	private void CreateDB(){
		col = 3;
		row = 5;
		size = height/7;
		gap = size/3;
		offset_x = (width - size*col - gap*(col - 1))/2;
		offset_y = height - size -(height - size*row - gap*(row-1))/2;
		Levels = new TextButton[col*row];
		Lever_Style = new TextButtonStyle[col*row];
	}
	
	private void LoadImage(){
		atlas = new TextureAtlas(Gdx.files.internal("data/imageatlas.txt"));
		skin = new Skin(atlas);
		skin.addRegions(BlockGame.atlas_icon);
		int max_level;
		if(BlockGame.frefs.getInteger(BlockGame.GameType) == BlockGame.CROSSWORD)
			max_level = BlockGame.frefs.getInteger(BlockGame.LEVEL_CROSSWORD);
		else
			max_level = BlockGame.frefs.getInteger(BlockGame.LEVEL_OLIMPIA);
		for(int i=0 ; i<col*row ; i++){
			final int a = i + 1;
			Lever_Style[i] = new TextButtonStyle();
			if( i <= max_level){
				Lever_Style[i].up = skin.getDrawable("btnlevel");
				Lever_Style[i].down = skin.getDrawable("btnlevelpressed");
			}else{
				Lever_Style[i].up = skin.getDrawable("btnbatlocked");
			}
			Lever_Style[i].font = BlockGame.font;
			
			final int tmp = max_level;
			Levels[i] = new TextButton((i+1) + "",Lever_Style[i]);
			Levels[i].setSize(size , size);
			Levels[i].setPosition(offset_x + (i%col) * (size + gap), offset_y - (i/col) * (gap + size));
			Levels[i].addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y) {
					sound_Menu.play();
					if(a <= (tmp + 1)){
//						int level = (a < 4 ? a : 3);
						int level = a;
						ScreenManager.inst().game.setScreen(ScreenManager.inst().getplayScreen(level));						
					}
					
				}
			});
			stage.addActor(Levels[i]);
		}
//		CreateButton();
	}
	
	private void CreateButton(){
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable("BackGround");
		style.down = skin.getDrawable("none_image");
		style.font = BlockGame.font_TiengViet;
		style.fontColor = Color.WHITE;
		btn_back = new TextButton("Quay Trở Lại", style);
		btn_back.setSize(VIEWPORT_WIDTH/3, VIEWPORT_HEIGHT/18);
		btn_back.setPosition(0, 0);
		btn_back.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// TODO Auto-generated method stub
				Dialog dialog = new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json") ,new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))){
					protected void result(Object object) {
						if(object.toString() == "true")
							ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuScreen());
					};
				}.text("Quay Trở Lại ?").button("Yes", true).button("No", false);
				dialog.setWidth(VIEWPORT_WIDTH/2);
				dialog.setPosition(VIEWPORT_WIDTH/4 , VIEWPORT_HEIGHT*2/3);
				dialog.show(stage);
			}
		});
		stage.addActor(btn_back);		
	}
	
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		super.render(delta);
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {
		super.hide();
	}
}
