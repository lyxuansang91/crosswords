package com.demo.crosswords.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.demo.crosswords.BaseScreen;
import com.demo.crosswords.BlockGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 480;
		config.height = 853;
		new LwjglApplication(new BlockGame(), config);
	}
}
