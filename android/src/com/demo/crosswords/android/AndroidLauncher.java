package com.demo.crosswords.android;

import android.content.Intent;
import android.os.Bundle;

import android.util.Log;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.demo.crosswords.BaseScreen;
import com.demo.crosswords.BlockGame;

public class AndroidLauncher extends AndroidApplication implements BlockGame.AndroidOnlyInterface {

	private static final String TAG = AndroidLauncher.class.getSimpleName();
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		BlockGame blockGame = new BlockGame();
		blockGame.setAndroidOnlyInterface(this);
		initialize(blockGame, config);
	}

	@Override
	public void shareFacebook(int mark) {
		Log.d(TAG, "share facebook");
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_TEXT, "your mark:" + mark);
		startActivity(Intent.createChooser(intent, "Share using..."));
	}
}
