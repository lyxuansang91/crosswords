package com.demo.crosswords;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.demo.crosswords.models.OlimpiaMap;

public class OlimpiaGroup extends Group {

	private ImageButton Keybroad[];
	private TextButton btn[] , btn_ShowAnswer , btn_CheckAnswer;
	private TextButtonStyle btn_style[];
	private Skin skin;
	private Stage stage;
	private SpriteBatch batch;
	private Label clues , topic;
	private TextureRegion Timer[];
	private Image person , BackgroundKeybroad;
	private Sound sound_Keybroads , sound_win , sound_fail;
	private OlimpiaMap crosswordMap;
	private int levels , size , col , row , index , offsetX , offsetY , gapX , size_timer;
	private int click[] ,height , width;
	private float time;
	public boolean select_down , isFinish , isWin;

	public OlimpiaGroup(float widthscreen, float heightScreen , int _levels , Stage _stage) {
		super();
		this.levels = _levels;
		this.stage = _stage;
		height = BaseScreen.VIEWPORT_HEIGHT;
		width = BaseScreen.VIEWPORT_WIDTH;
		this.setWidth(width);
		this.setHeight(height);
		newdata();
		initMenu(widthscreen, heightScreen);
	}

	private void newdata() {
		time = 300;
		size_timer = width/30;
		crosswordMap = OlimpiaMap.LoadDB(levels);
		col = crosswordMap.size.cols;
		row = crosswordMap.size.rows;
		size = width/(col*3/2);
		offsetX = width - size*col;
		offsetY = height/2;
		click = new int[col*row];
		btn = new TextButton[col*row];
		btn_style = new TextButtonStyle[col*row];
		Keybroad = new ImageButton[BlockGame.Keybroad.length];
		skin = new Skin(BlockGame.atlas_icon);
		skin.addRegions(BlockGame.atlas_Keybroad);
		gapX = (width - 7*width/10)/8;
		select_down = isFinish = isWin = false;
		index = -1;
		Timer = new TextureRegion[5];
		sound_fail = BlockGame.sound_fail;
		sound_win = BlockGame.sound_win;
		sound_Keybroads = BlockGame.sound_Stage;
	}

	private void initMenu(float widthscreen, float heightScreen) {
		
		TextureRegion region = BlockGame.atlas_actor.findRegion(crosswordMap.imagetopic);
		region.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		person = new Image(region);
		person.setSize(offsetX , (region.getRegionHeight()*offsetX)/region.getRegionWidth());
		person.setPosition(0, offsetY);
		this.addActor(person);
		
		region = new TextureRegion();
		region.setRegion(BlockGame.atlas_icon.findRegion("BackGroundKeybroad"));
		region.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		BackgroundKeybroad = new Image(region);
		BackgroundKeybroad.setSize(width, height/18 + ((BlockGame.Keybroad.length - 1)/7 + 1)*width/10 + 20);
		BackgroundKeybroad.setPosition(0, 0);
		this.addActor(BackgroundKeybroad);
		
		LabelStyle style_topic = new LabelStyle(BlockGame.font_TiengViet, Color.WHITE);
		topic = new Label(crosswordMap.topic, style_topic);
		topic.setWrap(true);
		topic.setPosition((width - topic.getWidth())/2 , offsetY + row*size + 5);
		this.addActor(topic);
		
		for(int i=0 ; i< crosswordMap.grid.length ; i++){
			if(crosswordMap.grid[i].compareTo(".") == 0){
				click[i] = 1;
			}else{
				if(i%col == crosswordMap.down)
					click[i] = 2;
			}
		}

		for (int i = 0; i < col * row; i++) {
			final int x = i % col;
			final int y = i / col;
			final int n = i;
			btn_style[i] = new TextButtonStyle();
			if(click[i] == 0)
				btn_style[i].up = skin.getDrawable("white");
			else if(click[i] == 2)
				btn_style[i].up = skin.getDrawable("pink");
			else
				btn_style[i].up = skin.getDrawable("null");
			btn_style[i].font = BlockGame.font;
			btn_style[i].fontColor = Color.BLACK;
			btn[i] = new TextButton("", btn_style[i]);
			btn[i].setSize(size - 2f, size - 2f);
			btn[i].setPosition(offsetX + x * (size), offsetY + y * (size));
			btn[i].addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y) {
					sound_Keybroads.play();
					if(crosswordMap.grid[n].compareTo(".") != 0){
						index = n;
						for(int j = 0 ; j<col*row ; j++){
							btn[j].addAction(Actions.color(Color.WHITE));
						}
						CheckGroupCheck(n);
					}
					super.clicked(event, x, y);
				}
			});
			this.addActor(btn[i]);
		}

		// Create Charter
		for(int i=0 ; i<BlockGame.Keybroad.length ; i++){
			int x = i % 7;
			int y = i / 7;
			final int a = i;
			ImageButtonStyle style = new ImageButtonStyle();
			style.up = skin.getDrawable("ic_key_" + BlockGame.Keybroad[i]);
			style.down = skin.getDrawable("ic_key_" + BlockGame.Keybroad[i] + "_focused");
			Keybroad[i] = new ImageButton(style);
			Keybroad[i].setSize(width/10, width/11);
			Keybroad[i].setPosition(gapX + (width/10 + gapX)*x, width*y/10 + height/18 + 10);
			Keybroad[i].addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y) {
					// TODO Auto-generated method stub
					super.clicked(event, x, y);
					sound_Keybroads.play();
					if(index >=0){
						if(BlockGame.Keybroad[a].compareTo("back") == 0){
							if(index>=1){
								if(btn[index].getText().toString().compareTo("") != 0){
									btn[index].setText("");
								}else if(crosswordMap.grid[index-1].compareTo(".") != 0){
									index--;
									btn[index].setText("");
								}
								CheckGroupCheck(index);
							}else if(index == 0 && crosswordMap.grid[0].compareTo(".") != 0){
								if(btn[index].getText().toString().compareTo("") != 0){
									btn[index].setText("");
								}
							}
						}else if (BlockGame.Keybroad[a].compareTo("ok") == 0){
							
							new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json") ,new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))){
								protected void result(Object object) {
									if(object.toString() == "true"){
//										CheckEndGame();
										if(CheckFinish())
											ShowDialog(true , (300 - (int)time));
										else
											ShowDialog(false, 0);
									}
								};
							}.text("Đây Là Đáp Án Cuối Cùng ?").button("Vâng", "true").button("Không", "false").show(stage);
						}else{
							if(index%col == crosswordMap.down)
								btn_style[index].fontColor = Color.WHITE;
							btn[index].setText(BlockGame.Keybroad[a]);
							if(crosswordMap.grid[index+1].compareTo(".") != 0){
								index++;
								CheckGroupCheck(index);
							}
						}
					}
				}
			});
			this.addActor(Keybroad[i]);
		}
		setBottom();
	}
	
	private void setBottom(){
		batch = new SpriteBatch();
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable("BackGround");
		style.down = skin.getDrawable("none_image");
		style.font = BlockGame.font_TiengViet;
		btn_ShowAnswer = new TextButton("Show Answer", style);
		btn_ShowAnswer.setSize(BaseScreen.VIEWPORT_WIDTH / 3, BaseScreen.VIEWPORT_HEIGHT / 18);
		btn_ShowAnswer.setPosition(BaseScreen.VIEWPORT_WIDTH/3, 0);
		btn_ShowAnswer.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				sound_Keybroads.play();
				new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json"), new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))) {
					protected void result(Object object) {
						if(object.toString() == "true"){
							isFinish = true;
							for (int i = 0; i < col * row; i++) {
								if (crosswordMap.grid[i].compareTo(".") != 0)
									btn[i].setText(crosswordMap.grid[i]);
							}
							ShowDialog(false, 0);
						}
					}

					;
				}.text("Show All Anser ? ").button("Yes", true).button("No", false).show(stage);
			}
		});
		
		btn_CheckAnswer = new TextButton("Check Answer", style);
		btn_CheckAnswer.setSize(BaseScreen.VIEWPORT_WIDTH / 3, BaseScreen.VIEWPORT_HEIGHT / 18);
		btn_CheckAnswer.setPosition(BaseScreen.VIEWPORT_WIDTH*2/3, 0);
		btn_CheckAnswer.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				sound_Keybroads.play();
				if(index > -1){
					int i = index/col;
					CheckRowAnswer(i);
				}
			}
		});
		
		this.addActor(btn_CheckAnswer);
		this.addActor(btn_ShowAnswer);
	}
	
	public void render(float deltatime){
		batch.begin();
		btn_CheckAnswer.draw(batch, 1);
		btn_ShowAnswer.draw(batch, 1);
		if(index>-1)
			clues.draw(batch, 1);
		if(!isFinish)
			setTime(deltatime);
		batch.end();
	}
	
	private void CheckGroupCheck(int _position){
		int n = _position/col;
		for(int i= n*col ; i<(n+1)*col ; i++){
			btn[i].addAction(Actions.color(Color.CYAN));
		}
		btn[_position].addAction(Actions.color(Color.GRAY));
		LoadClues(n);
	}
	
	private void LoadClues(int position){
		OlimpiaGroup.this.removeActor(clues);
		LabelStyle style = new LabelStyle(BlockGame.font_unicode, Color.WHITE);
		clues = new Label(crosswordMap.hint[position], style);
		clues.setWidth(width*6/7);
		clues.setWrap(true);
		clues.setPosition(width / 14, height/18 + BackgroundKeybroad.getHeight());
		this.addActor(clues);
	}
	
	private boolean CheckFinish(){
		int j = crosswordMap.down;
		boolean check = true;
		for(int i = 0 ; i < row ; i++ , j = j+col){
			if(btn[j].getText().toString().toUpperCase().compareTo(crosswordMap.grid[j]) != 0){
				check = false;
				if(btn[j].getText().toString().compareTo("") != 0){
					btn[j].addAction(Actions.color(Color.RED));
				}
			}
		}
		return check;
	}
	
	private void setTime(float delta){
		if(!isFinish)
			time = time - delta;
		int number_time[] = new int[5];
		int p = (int)time/60;
		number_time[1] = p%10;
		number_time[0] = p / 10;
		int s = (int) time%60;
		number_time[4] = s % 10;
		number_time[3] = s / 10;
		number_time[2] = 0;
		for(int i=0 ; i<5 ; i++){
			if(i == 2){
				Timer[i] = new TextureRegion(BlockGame.atlas_icon.findRegion("HaiCham"));
			}else{
				Timer[i] = new TextureRegion(BlockGame.atlas_icon.findRegion(number_time[i]+""));
				Timer[i].getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			}
			batch.draw(Timer[i],i*size_timer , offsetY + size*row , size_timer, size_timer);
		}
		if(time <= 0){
			isFinish = true;
			ShowDialog(false, 0);
		}
	}

	private void shareFacebook(int mark) {
		BlockGame.AndroidOnlyInterface androidOnlyInterface = ScreenManager.inst().game.getiAndroidOnlyInterface();
		if(androidOnlyInterface != null)
			androidOnlyInterface.shareFacebook(mark);
	}
	
	private void ShowDialog(final boolean _isWin , final int _Markss){
		if(_isWin){
			sound_win.play();
			int max_level = BlockGame.frefs.getInteger(BlockGame.LEVEL_OLIMPIA);
			if(levels == (max_level + 1))
				max_level++;
			BlockGame.frefs.putInteger(BlockGame.LEVEL_OLIMPIA, max_level);
			BlockGame.frefs.flush();
		}
		else
			sound_fail.play();
		isFinish = true;
		Dialog dialog = new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json") ,new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))){
			protected void result(Object object) {
				if(_isWin)
					sound_win.stop();
				else
					sound_fail.stop();
				if(object.toString() == "1"){
					ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuLeverScreen());
				}else if( object.toString() == "3"){
					if(levels < 2)
						levels++;
					ScreenManager.inst().game.setScreen(ScreenManager.inst().getplayScreen(levels));
				} else {
					shareFacebook(_Markss);
					ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuLeverScreen());
				}
			};
		}.button("Back", "1");
		if(_isWin)
			dialog.button(" Share ","2").button("Next", "3").text(_Markss + "s");
		else
			dialog.text("Bạn Đã Thua Cuộc");
		dialog.show(stage);
	}
	
	private boolean CheckRowAnswer(int row_index){
		boolean check = true;
		for(int i = row_index*col ; i < col*(row_index + 1) ; i++){
			if(crosswordMap.grid[i].compareTo(".") != 0 && btn[i].getText().toString().compareTo("") != 0 && btn[i].getText().toString().toUpperCase().compareTo(crosswordMap.grid[i]) != 0){
				btn[i].addAction(Actions.color(Color.RED));
				check = false;
			}else{
				btn[i].addAction(Actions.color(Color.CYAN));
			}
		}
		btn[index].addAction(Actions.color(Color.GRAY));
		return check;
	}
}
