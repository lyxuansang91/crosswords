package com.demo.crosswords.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.demo.crosswords.BaseScreen;
import com.demo.crosswords.BlockGame;
import com.demo.crosswords.ScreenManager;

public class MenuScreen extends BaseScreen {

	private TextButton btn_start , btn_exit , btn_setting;
	private TextButtonStyle btn_style;
	private TextureAtlas atlas;
	private Skin skin;
	private BitmapFont font;
	private Sound soundButton;
	private int height , width , offset_x , offset_y , gap , size;
	
	@Override
	public void show() {
		super.show();
		height = BaseScreen.VIEWPORT_HEIGHT;
		width = BaseScreen.VIEWPORT_WIDTH;
		size = height/10;
		gap = size/2;
		offset_x = (width - size*2)/2;
		offset_y = (height - size) / 2;
		atlas = new TextureAtlas(Gdx.files.internal("data/imageatlas.txt"));
		font = new BitmapFont(Gdx.files.internal("font/black_tiengviet.fnt") , false);
		skin = new Skin(atlas);
		soundButton = BlockGame.sound_Stage;
		
		LoadImage();
	}
	
	private void LoadImage(){
		btn_style = new TextButtonStyle();
		btn_style.up = skin.getDrawable("btnallmenu");
		btn_style.down = skin.getDrawable("btnallmenupressed");
		btn_style.font = font;
		btn_start = new TextButton("Start", btn_style);
		btn_start.setSize(size*2, size);
		btn_start.setPosition(offset_x, offset_y + gap + size);
		btn_start.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				soundButton.play();
				ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuLeverScreen());
			}
		});
		
		btn_exit = new TextButton("Exit", btn_style);
		btn_exit.setSize(size * 2, size);
		btn_exit.setPosition(offset_x, offset_y - gap - size);
		btn_exit.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				soundButton.play();
				Gdx.app.exit();
			}
		});
		
		btn_setting = new TextButton("Setting", btn_style);
		btn_setting.setSize(size*2, size);
		btn_setting.setPosition(offset_x, offset_y);
		btn_setting.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				soundButton.play();
				ScreenManager.inst().game.setScreen(ScreenManager.inst().getSettingScreen());
			}
		});
		stage.addActor(btn_start);
		stage.addActor(btn_setting);
		stage.addActor(btn_exit);
	}
	
	@Override
	public void render(float delta) {
		super.render(delta);
	}
	
	@Override
	public void resize(int w, int h) {
		super.resize(w, h);
	}

}
