package com.demo.crosswords;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class BlockGame extends Game {
	public static BitmapFont font , font_TiengViet , font_unicode;
	public static TextureAtlas atlas_icon , atlas_Keybroad , atlas_actor;
	public static String Keybroad[] = {"z" , "x" , "c" , "v" , "b" , "n" , "m" , "a" , "s" , "d" , "f" , "g" , "h" , "j" , "k" , "l" , "q" , "w" , "e" , "r" , "t" , "y" , "u" , "i" , "o" , "p" , "back", "ok"};
	public static Sound sound_menuBasic , sound_Stage , sound_win , sound_fail , sound_dialog , sound_keysbroad;
	public static Preferences frefs;
	public static final int CROSSWORD = 1;
	public static final int OLIMPIA = 2;
	public static final String GameType = "GameType";
	public static final String LEVEL_CROSSWORD = "Level_Crossword";
	public static final String LEVEL_OLIMPIA = "Level_Olimpia";

	public BlockGame() {
	}

	public interface AndroidOnlyInterface {
		public void shareFacebook(int mark);
	}

	private AndroidOnlyInterface iAndroidOnlyInterface;

	public AndroidOnlyInterface getiAndroidOnlyInterface() {
		return iAndroidOnlyInterface;
	}

	public void setAndroidOnlyInterface(AndroidOnlyInterface iAndroidOnlyInterface) {
		this.iAndroidOnlyInterface = iAndroidOnlyInterface;
	}

	@Override
	public void create() {
		
		ScreenManager.inst().initialize(this);
		
		font = new BitmapFont(Gdx.files.internal("font/font25.fnt"),Gdx.files.internal("font/font25.png"), false);
		font_TiengViet = new BitmapFont(Gdx.files.internal("font/black_tiengviet.fnt"),Gdx.files.internal("font/black_tiengviet.png"), false);
		font_unicode = new BitmapFont(Gdx.files.internal("black_tiengviet.fnt"),Gdx.files.internal("black_tiengviet.png"), false);
		font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		atlas_actor= new TextureAtlas(Gdx.files.internal("data/Actor.atlas"));
		atlas_icon = new TextureAtlas(Gdx.files.internal("data/Icon.atlas"));
		atlas_Keybroad = new TextureAtlas(Gdx.files.internal("data/Keybroad.atlas"));
		
		sound_dialog = Gdx.audio.newSound(Gdx.files.internal("sound/dialog.wav"));
		sound_fail = Gdx.audio.newSound(Gdx.files.internal("sound/fail.wav"));
		sound_win = Gdx.audio.newSound(Gdx.files.internal("sound/win.mp3"));
		sound_Stage = Gdx.audio.newSound(Gdx.files.internal("sound/MenuStage.wav"));
		
		frefs = Gdx.app.getPreferences("Crossword.prefs");
		frefs.putInteger(GameType, CROSSWORD);
		frefs.flush();
		frefs.getInteger(LEVEL_CROSSWORD, 0);
		frefs.getInteger(LEVEL_OLIMPIA, 0);

		ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuScreen());
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		super.pause();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		super.resume();
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		super.resize(width, height);
	}

}
