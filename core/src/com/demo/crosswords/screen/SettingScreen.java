package com.demo.crosswords.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.demo.crosswords.BaseScreen;
import com.demo.crosswords.BlockGame;
import com.demo.crosswords.ScreenManager;

public class SettingScreen extends BaseScreen{

	private Label title;
	private TextButton btn_ok;
	private CheckBox Check_Crossword, Check_Olimpia;
	private Skin skin;
	private int height , width;
			
	@Override
	public void show() {
		super.show();
		height = BaseScreen.VIEWPORT_HEIGHT;
		width =  BaseScreen.VIEWPORT_WIDTH;

		skin = new Skin(new TextureAtlas(Gdx.files.internal("data/imageatlas.txt")));
		skin.addRegions(BlockGame.atlas_icon);
		CreateCheckBox();
		CreateButton();
		CreateLabel();
	}
	
	private void CreateCheckBox(){
		stage.clear();
		int tmp=  BlockGame.frefs.getInteger(BlockGame.GameType);
		CheckBoxStyle check_style = new CheckBoxStyle();
		check_style.checkboxOff = skin.getDrawable("UnCheckBox");
		check_style.checkboxOn = skin.getDrawable("CheckBox");
		check_style.font = BlockGame.font;
		check_style.fontColor = Color.BLACK;
		
		Check_Crossword = new CheckBox("Crossword", check_style);
		Check_Crossword.setPosition(width/4 ,height/2 + Check_Crossword.getHeight()/2);
		if(tmp == BlockGame.CROSSWORD)
			Check_Crossword.setChecked(true);
		Check_Crossword.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(Check_Crossword.isChecked())
					Check_Olimpia.setChecked(false);
				else
					Check_Olimpia.setChecked(true);
			}
		});

		Check_Olimpia = new CheckBox("Olimpia", check_style);
		Check_Olimpia.setPosition(width/4 ,height/2 - Check_Crossword.getHeight()/2);
		if(tmp == BlockGame.OLIMPIA)
			Check_Olimpia.setChecked(true);
		Check_Olimpia.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(Check_Olimpia.isChecked())
					Check_Crossword.setChecked(false);
				else
					Check_Crossword.setChecked(true);
			}
		});
		
		
		
		stage.addActor(Check_Crossword);
		stage.addActor(Check_Olimpia);
	}
	
	private void CreateButton(){
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable("btnallmenu");
		style.down = skin.getDrawable("btnallmenupressed");
		style.font = BlockGame.font;
		style.fontColor = Color.BLACK;
		btn_ok = new TextButton("OK", style);
		btn_ok.setSize(width/3, height/10);
		btn_ok.setPosition(width/3, Check_Olimpia.getY() - Check_Olimpia.getHeight()*3/2);
		btn_ok.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(Check_Crossword.isChecked())
					BlockGame.frefs.putInteger(BlockGame.GameType, BlockGame.CROSSWORD);
				else
					BlockGame.frefs.putInteger(BlockGame.GameType, BlockGame.OLIMPIA);
				BlockGame.frefs.flush();
				ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuScreen());
			}
		});
		stage.addActor(btn_ok);
	}
	
	private void CreateLabel(){
		LabelStyle label_style = new LabelStyle();
		label_style.font = BlockGame.font;
		label_style.fontColor = Color.BLACK;
		title = new Label("Select Game Type", label_style);
		title.setPosition((width - title.getWidth())/2, Check_Crossword.getY() + Check_Crossword.getHeight()*3/2);
		
		stage.addActor(title);
	}
}
