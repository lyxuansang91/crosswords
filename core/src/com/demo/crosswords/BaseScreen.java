package com.demo.crosswords;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.demo.crosswords.screen.MenuLevelScreen;
import com.demo.crosswords.screen.MenuScreen;

public class BaseScreen implements Screen, InputProcessor {

	public static int VIEWPORT_WIDTH = Gdx.graphics.getWidth(), VIEWPORT_HEIGHT = Gdx.graphics.getHeight();
	public Stage stage;
	public boolean isChangeScreen = false;
	private OrthographicCamera camera;
	public static Sprite background;
	public SpriteBatch batch;
	boolean isBackButtonActive;
	public InputMultiplexer inputMultiplexer;

//	Camera camera;

	public BaseScreen() {
		stage = new Stage(new FitViewport(VIEWPORT_WIDTH, VIEWPORT_HEIGHT));
		this.inputMultiplexer = new InputMultiplexer(this);
		inputMultiplexer.addProcessor(stage);
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, VIEWPORT_WIDTH, VIEWPORT_HEIGHT);
	}

	@Override
	public void render(float delta) {
		Gdx.input.setCatchBackKey(true);
		Gdx.gl20.glClearColor(1,1,1,1); // ff7e00
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.combined);
		batch.begin();	
		background.draw(batch);
		batch.end();
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int w, int h) {

		stage.setViewport(new FitViewport(VIEWPORT_WIDTH, VIEWPORT_HEIGHT));
		stage.getViewport().update(w, h, true);

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		Gdx.input.setInputProcessor(inputMultiplexer);
		Texture texture = new Texture(Gdx.files.internal("BackGround.jpg"));
		texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		TextureRegion background_region = new TextureRegion(texture ,0 , 0 , texture.getWidth(),texture.getHeight());
		background = new Sprite(background_region);
		background.setSize(VIEWPORT_WIDTH , VIEWPORT_HEIGHT);
		background.setPosition(0, 0);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		this.dispose();
		Gdx.app.log("BaseScreen", "dispose");
	}

	public void keyBackPressed() {
		Gdx.app.log("BaseScreen", "keyBackPressed");
	}

	public void setActiveBackButton(boolean _isActiveBackButton) {
		this.isBackButtonActive = _isActiveBackButton;
		Gdx.input.setCatchBackKey(isBackButtonActive);
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		if(keycode == Keys.BACK || keycode == Keys.BACKSLASH || keycode == Keys.BACKSPACE)
			if(this.getClass() == MenuLevelScreen.class)
				ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuScreen());
			else if(this.getClass() == PlayScreen.class){
				Dialog dialog = new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json") ,new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))){
					protected void result(Object object) {
						if(object.toString() == "true")
							ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuLeverScreen());
					};
				}.text("Stop Game ?").button("Yes", "true").button("No", "false");
				dialog.setPosition(0, 0);
				dialog.show(stage);
			}
			else if(this.getClass() == MenuScreen.class){
				new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json") ,new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))){
					protected void result(Object object) {
						if(object.toString() == "true")
							Gdx.app.exit();
					};
				}.text("Exit Game ?").button("Yes", "true").button("No", "false").show(stage);
			}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		if (keycode == Keys.BACK) {
			if (isBackButtonActive) {
				keyBackPressed();
			}
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
