package com.demo.crosswords;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class PlayScreen extends BaseScreen {

	private int levels , gametype;
	private OlimpiaGroup olimpiaGroup;
	private CrosswordGroup crossWord;
	private TextButton btn_back;
	private Skin skin;
	private NinePatch Botton;

	public PlayScreen(int _levels) {
		this.levels = _levels;
	}
	
	@Override
	public void show() {
		super.show();

		float widthscreen = stage.getCamera().viewportWidth;
		float heightScreen = stage.getCamera().viewportHeight;
		background.setSize(VIEWPORT_WIDTH, VIEWPORT_HEIGHT/2 + 2);
		background.setPosition(0, VIEWPORT_HEIGHT/2 - 2);
		
		gametype = BlockGame.frefs.getInteger(BlockGame.GameType);
		if(gametype == BlockGame.CROSSWORD){
			crossWord = new CrosswordGroup(widthscreen, heightScreen, levels, stage);
			background.setSize(VIEWPORT_WIDTH, VIEWPORT_HEIGHT*11 / 20 + 4);
			background.setPosition(0, VIEWPORT_HEIGHT * 9 / 20 - 2);
			stage.addActor(crossWord);
		}else{
			olimpiaGroup = new OlimpiaGroup(widthscreen, heightScreen , levels , stage);
			stage.addActor(olimpiaGroup);
		}
		LoadImage();
	}
	
	private void LoadImage(){
		skin = new Skin(BlockGame.atlas_icon);
		Botton = new NinePatch(BlockGame.atlas_icon.findRegion("BackGround"));
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable("BackGround");
		style.down = skin.getDrawable("none_image");
		style.font = BlockGame.font_TiengViet;
		style.fontColor = Color.WHITE;
		btn_back = new TextButton("Quay Trở Lại", style);
		btn_back.setSize(VIEWPORT_WIDTH/3, VIEWPORT_HEIGHT/18);
		btn_back.setPosition(0, 0);
		btn_back.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// TODO Auto-generated method stub
				Dialog dialog = new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json") ,new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))){
					protected void result(Object object) {
						if(object.toString() == "true")
							ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuLeverScreen());
					};
				}.text("Exit Game ?").button("Yes", true).button("No", false);
				dialog.setWidth(VIEWPORT_WIDTH/2);
				dialog.setPosition(VIEWPORT_WIDTH/4 , VIEWPORT_HEIGHT*2/3);
				dialog.show(stage);
			}
		});
		stage.addActor(btn_back);
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		batch.begin();
		Botton.draw(batch, 0, 0, VIEWPORT_WIDTH, VIEWPORT_HEIGHT/18);
		if(gametype == BlockGame.OLIMPIA)
			olimpiaGroup.render(delta);
		else
			crossWord.render(delta);
		btn_back.draw(batch, 1);
		batch.end();
	}
	
	@Override
	public void resize(int w, int h) {
		super.resize(w, h);
	}
	
	@Override
	public void hide() {
		super.hide();
	}

}
