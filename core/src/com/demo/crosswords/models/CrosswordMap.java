package com.demo.crosswords.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

public class CrosswordMap {
	
	public Groups groups;
	public String grid[];
//	public CrosswordMap.Answer answers;
	public CrosswordMap.Clue clues;
	public CrosswordMap.Size size;
	public String topic;
	public String image;
	
	public static CrosswordMap LoadDB(int level){
		FileHandle file = Gdx.files.internal(String.format("map/crossword_%d.json", level));
		Json json = new Json();
		CrosswordMap demo = json.fromJson(CrosswordMap.class, file);
		return demo;
	}
	
	public static class Answer{
		public String down[];
		public String across[];
	}
	
	public static class Clue{
		public String across[];
		public String down[];
	}
	
	public static class Size{
		public int cols;
		public int rows;
	}
	
	public static class Groups{
		public int across[][];
		public int down[][];
	}
}
