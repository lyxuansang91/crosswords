package com.demo.crosswords;

public class Data {
	public static String[] index = { "1", "3", "5", "7", "13", "17", "22",
			"23", "25", "35", "11" };
	public static String[] Character = { "A", "B", "C", "D", "E", "F", "G",
			"H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
			"U", "V", "W", "X", "Y", "Z" };
	public static int[] group_board = { 1, 32, 27, 20, 33, 34, 17 };
	public static int[] group_1 = {0,6,12,18,24,30};
	public static int[] group_2 = {31,32,33,34};
	public static int[] group_3 = {26,27,28,29};
	public static int[] group_4 = {19,20,21};
	public static int[] group_5 = {2,8,14,20,26,32};
	public static int[] group_6 = {9,15,21,27,33};
	public static int[] group_7 = {4,10,16};
}
