package com.demo.crosswords;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.demo.crosswords.models.CrosswordMap;

public class CrosswordGroup extends Group {
	
	private ImageButton Keybroad[];
	private Image BackgroundKeybroad;
	private Skin skin;
	private SpriteBatch batch;
	private Stage stage;
	private TextButton btn_Grid[] , btn_CheckAnswer , btn_ShowAnswer , btn;
	private TextButtonStyle btn_grid_style[];
	private Label clues , topic;
	private Image person;
	private NinePatch frameTopic;
	private CrosswordMap crosswordMap;
	private Sound sound_Keybroads , sound_win , sound_fail;
	
	private int width , height , index , level , col , row , count_answer , max_answer;
	private boolean SelectDown , Finish , CheckAnswer;
	
	public CrosswordGroup(float widthscreen, float heightScreen , int _levels , Stage _stage) {
		super();
		this.level = _levels;
		this.stage = _stage;
		width = BaseScreen.VIEWPORT_WIDTH;
		height = BaseScreen.VIEWPORT_HEIGHT;
		this.setWidth(width);
		this.setHeight(height);
		newdata();
		initMenu(widthscreen, heightScreen);
	}

	private void newdata() {
		crosswordMap = CrosswordMap.LoadDB(level);
		Keybroad = new ImageButton[BlockGame.Keybroad.length];
		skin = new Skin(BlockGame.atlas_icon);
		skin.addRegions(BlockGame.atlas_Keybroad);
		
		col = crosswordMap.size.cols;
		row = crosswordMap.size.rows;
		
		btn_Grid = new TextButton[col*row];
		btn_grid_style = new TextButtonStyle[col*row];
		
	}

	private void initMenu(float widthscreen, float heightScreen) {
		sound_fail = BlockGame.sound_fail;
		sound_win = BlockGame.sound_win;
		sound_Keybroads = BlockGame.sound_Stage;
		
		index = -1;
		count_answer = 0;
		max_answer = 0;
		SelectDown = false;
		Finish = false;
		
		for(int i=0 ; i<col*row ; i++){
			if(crosswordMap.grid[i].compareTo(".") != 0 )
				max_answer++;
		}
		CreateKeyBroad();
		CreateGrid();
		CreateTopic();
		setBottom();
	}

	private void CreateKeyBroad(){
		// Tao Background KeyBroad
		TextureRegion region = new TextureRegion();
		region.setRegion(BlockGame.atlas_icon.findRegion("BackGroundKeybroad"));
		region.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		BackgroundKeybroad = new Image(region);
		BackgroundKeybroad.setSize(width, height/18 + ((BlockGame.Keybroad.length - 1)/7 + 1)*width/12 + 2);
		BackgroundKeybroad.setPosition(0, 0);
		this.addActor(BackgroundKeybroad);
		
		// Tao KeyBroad
		int gapX = (width - 7*width/10)/8; // khoang cach giua 2 phim
		for(int i=0 ; i<(BlockGame.Keybroad.length - 1) ; i++){
			int x = i % 7;
			int y = i / 7;
			final int a = i;
			ImageButtonStyle style = new ImageButtonStyle();
			style.up = skin.getDrawable("ic_key_" + BlockGame.Keybroad[i]);
			style.down = skin.getDrawable("ic_key_" + BlockGame.Keybroad[i] + "_focused");
			Keybroad[i] = new ImageButton(style);
			Keybroad[i].setSize(width/10, width/13);
			Keybroad[i].setPosition(gapX + (width/10 + gapX)*x, width*y/12 + height/18 + 2);
			Keybroad[i].addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y) {
					sound_Keybroads.play();
					if(index > -1 && !Finish){
						// bat su kien call Back
						if(BlockGame.Keybroad[a].compareTo("back") == 0){
							if(btn_Grid[index].getText().toString().compareTo("") != 0){
								btn_Grid[index].setText("");
								count_answer--;
							}else{
								if(!SelectDown){
									if(index > 0 && (index - 1)%col != (col - 1) && crosswordMap.grid[index - 1].compareTo("." )!= 0){
										index--;
										if(btn_Grid[index].getText().toString().compareTo("") !=0 )
											count_answer--;
										btn_Grid[index].setText("");
										CheckGroupCheck(index);
									}									
								}else{
									int tmp = index/col;
									if(tmp < row && (tmp - 1) >= 0 && crosswordMap.grid[index - col].compareTo("." )!= 0){
										index = index - col;
										if(btn_Grid[index].getText().toString().compareTo("") !=0 )
											count_answer--;
										btn_Grid[index].setText("");
										CheckGroupCheck(index);
									}
								}
							}
						}else{
							if(btn_Grid[index].getText().toString().compareTo("") == 0)
								count_answer++;
							btn_Grid[index].setText(BlockGame.Keybroad[a]);
							if(!SelectDown){
								if((index + 1) < (col*row) && crosswordMap.grid[index+1].compareTo(".") != 0 && (index + 1)%col != 0){
									index++;
									CheckGroupCheck(index);
								}
							}else{
								int tmp = index/col;
								if( tmp < (row - 1) && crosswordMap.grid[index+col].compareTo(".") != 0){
									index = index + col;
									CheckGroupCheck(index);
								}
							}
							CheckFinish();
						}
					}
				}
			});
			this.addActor(Keybroad[i]);
		}
	}
	
	private void CheckFinish(){
		CheckAnswer = true;
		if(count_answer == max_answer){
			for(int i=0 ; i<col*row ; i++){
				if(crosswordMap.grid[i].compareTo(".") != 0){
					if(btn_Grid[i].getText().toString().toUpperCase().compareTo(crosswordMap.grid[i]) != 0){
						CheckAnswer = false;
						break;
					}
				}
			}
			
			new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json") ,new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))){
				protected void result(Object object) {
					if(object.toString().compareTo("true") == 0){
						ShowDialog(CheckAnswer);
					}
				};
			}.text("Đây là đáp án cuối cùng ?").button("Đúng", "true").button("Không", "false").show(stage);
			
		}
	}
	
	private void setBottom(){
		batch = new SpriteBatch();
		TextButtonStyle style = new TextButtonStyle();
		style.up = skin.getDrawable("BackGround");
		style.down = skin.getDrawable("none_image");
		style.font = BlockGame.font_TiengViet;
		
		btn_ShowAnswer = new TextButton("Xem Kết Quả", style);
		btn_ShowAnswer.setSize(width / 3, height/ 18);
		btn_ShowAnswer.setPosition(width/3, 0);
		btn_ShowAnswer.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				sound_Keybroads.play();
				new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json"), new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))) {
					protected void result(Object object) {
						if(object.toString() == "true"){
							for(int i=0 ; i<col*row ; i++){
								if(crosswordMap.grid[i].compareTo(".") != 0){
									btn_Grid[i].setText(crosswordMap.grid[i]);
								}
							}
							ShowDialog(false);
						}
					}
					;
				}.text("Show All Anser ? ").button("Yes", true).button("No", false).show(stage);
			}
		});
		
		btn_CheckAnswer = new TextButton("Kiểm Tra Đáp Án", style);
		btn_CheckAnswer.setSize(width / 3, height / 18);
		btn_CheckAnswer.setPosition(width*2/3, 0);
		btn_CheckAnswer.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if(!Finish)
					CheckAnswer();
				else
					ScreenManager.inst().game.setScreen(ScreenManager.inst().getplayScreen(level));
			}
		});
		
		this.addActor(btn_CheckAnswer);
		this.addActor(btn_ShowAnswer);
	}
	
	private void CreateTopic(){
		// Create Image Topic
		float personWidth , personHeight;
		personWidth = width - (btn_Grid[0].getWidth() + 2) * (col);
		personHeight = (btn_Grid[0].getHeight()) * row;
		TextureRegion region = BlockGame.atlas_actor.findRegion(crosswordMap.image);
		region.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		person = new Image(region);
		if(region.getRegionHeight() < personHeight && region.getRegionWidth()<personWidth){
			person.setPosition((personWidth - person.getWidth())/2, height * 9 / 20 );
		}else{
			if(region.getRegionHeight()/personHeight > region.getRegionWidth()/personWidth){
				person.setSize(region.getRegionWidth() * personHeight/region.getRegionHeight(), personHeight);
				person.setPosition((personWidth - person.getWidth())/2, height * 9 / 20 );
			}else{
				person.setSize(personWidth, region.getRegionHeight() * personWidth/region.getRegionWidth());
				person.setPosition(0, height * 9 / 20 );
			}
		}
		
		this.addActor(person);
		
		// Create Title Topic
		LabelStyle style_topic = new LabelStyle(BlockGame.font_unicode, Color.WHITE);
		topic = new Label(crosswordMap.topic, style_topic);
		topic.setWrap(true);
		topic.setPosition((width - topic.getWidth())/2 , (height* 29 / 20 + row * (btn_Grid[0].getHeight() + 2))/2 - topic.getHeight());
		topic.setWidth(width - 20);
		this.addActor(topic);
		
		frameTopic = new NinePatch(BlockGame.atlas_icon.findRegion("frame") , 4 , 4 , 4, 4);
	}
	
	private void CheckAnswer(){
		if(!SelectDown){
			for(int i=0 ; i<crosswordMap.groups.across.length ; i++){
				for(int j = 0; j <crosswordMap.groups.across[i].length ; j++){
					if(crosswordMap.groups.across[i][j] == (index + 1)){
						for(int k = 0 ; k<crosswordMap.groups.across[i].length ; k++){
							int position = crosswordMap.groups.across[i][k] - 1;
							if(btn_Grid[position].getText().toString().compareTo("") != 0 && btn_Grid[position].getText().toString().toUpperCase().compareTo(crosswordMap.grid[position]) != 0){
								btn_Grid[position].addAction(Actions.color(Color.RED));
							}
						}
					}
				}
			}
		}else{
			for(int i=0 ; i<crosswordMap.groups.down.length ; i++){
				for(int j = 0; j <crosswordMap.groups.down[i].length ; j++){
					if(crosswordMap.groups.down[i][j] == (index + 1)){
						for(int k = 0 ; k<crosswordMap.groups.down[i].length ; k++){
							int position = crosswordMap.groups.down[i][k] - 1;
							if(btn_Grid[position].getText().toString().compareTo("") != 0 && btn_Grid[position].getText().toString().toUpperCase().compareTo(crosswordMap.grid[position]) != 0){
								btn_Grid[position].addAction(Actions.color(Color.RED));
							}
						}
					}
				}
			}
		}
	}
	
	private void CreateGrid(){
		float size = (float)(width * 2)/(col * 3);
		float offsetX = width - size*col - size/2;
		float offsetY = height * 9 / 20 + size*(row - 1);
		
		for(int i=0 ; i<col*row ; i++){
			final int n = i;
			int a = i%col;
			int b = i/col;
			
			btn_grid_style[i] = new TextButtonStyle();
			btn_grid_style[i].font = BlockGame.font_unicode;
			if(crosswordMap.grid[i].compareTo(".") == 0)
				btn_grid_style[i].up = skin.getDrawable("null");
			else
				btn_grid_style[i].up = skin.getDrawable("white");
			btn_Grid[i] = new TextButton("", btn_grid_style[i]);
			btn_Grid[i].setSize(size-1, size-1);
			btn_Grid[i].setPosition(offsetX + a * size, offsetY - b*size);
			btn_Grid[i].addListener(new ClickListener(){
				@Override
				public void clicked(InputEvent event, float x, float y) {
					if(crosswordMap.grid[n].compareTo(".") != 0 && !Finish){
						sound_Keybroads.play();
						if(index == n && !SelectDown)
							SelectDown = true;
						else SelectDown = false;
						CheckGroupCheck(n);
					}
				}
			});
			this.addActor(btn_Grid[i]);
		}	
	}
	
	private void ClearActions(){
		for(int j = 0 ; j<col*row ; j++){
			btn_Grid[j].addAction(Actions.color(Color.WHITE));
		}
	}
	
	private void CheckGroupCheck(int _position){
		boolean checkDown = true;
		if(!SelectDown){
			for(int i=0 ; i<crosswordMap.groups.across.length ; i++){
				for(int j = 0; j <crosswordMap.groups.across[i].length ; j++){
					if(crosswordMap.groups.across[i][j] == (_position + 1)){
						checkDown = false;
						ClearActions();
						for(int k = 0 ; k<crosswordMap.groups.across[i].length ; k++){
							btn_Grid[crosswordMap.groups.across[i][k] - 1].addAction(Actions.color(Color.GRAY));
						}
						btn_Grid[_position].addAction(Actions.color(Color.GRAY));
						LoadClues(i);
						break;
					}
				}
				if(!checkDown){
					break;
				}
			}
		}
		if(checkDown)
			SelectDown = true;
//		if(index == _position && !SelectDown)
//			SelectDown = true;
		if(SelectDown){
			for(int i=0 ; i<crosswordMap.groups.down.length ; i++){
				for(int j = 0; j <crosswordMap.groups.down[i].length ; j++){
					if(crosswordMap.groups.down[i][j] == (_position + 1)){
						ClearActions();
						for(int k = 0 ; k<crosswordMap.groups.down[i].length ; k++){
							btn_Grid[crosswordMap.groups.down[i][k] - 1].addAction(Actions.color(Color.GRAY));
						}
						btn_Grid[_position].addAction(Actions.color(Color.GRAY));
						LoadClues(i);
					}
				}
			}
		}
		index = _position;
	}

	private void LoadClues(int position){
		CrosswordGroup.this.removeActor(clues);
		LabelStyle style = new LabelStyle(BlockGame.font_unicode, Color.WHITE);
		if(SelectDown){
			clues = new Label(crosswordMap.clues.down[position], style);
		}else
			clues = new Label(crosswordMap.clues.across[position], style);
		clues.setWidth(BaseScreen.VIEWPORT_WIDTH - 20);
		clues.setWrap(true);
		int y = (int) ((height - width + BackgroundKeybroad.getHeight())/2);
		int x = (width - (int)clues.getWidth())/2;
		clues.setPosition(x , y);
		this.addActor(clues);
	}
	
	public void render(float deltatime){
		batch.begin();
		btn_CheckAnswer.draw(batch, 1);
		btn_ShowAnswer.draw(batch, 1);
		frameTopic.draw(batch , 0 , height*9/20 + btn_Grid[0].getHeight() * row + 10 , width , (height*11/20 - (btn_Grid[0].getHeight()) * row) - 10);
		batch.end();
	}
	
	private void ShowDialog(final boolean _isWin){
		for(int i=0 ; i<col*row ; i++){
			if(crosswordMap.grid[i].compareTo(".") != 0){
				if(btn_Grid[i].getText().toString().toUpperCase().compareTo(crosswordMap.grid[i]) != 0){
					btn_Grid[i].addAction(Actions.color(Color.RED));
				}
			}
		}
		
		if(!_isWin){
			btn_ShowAnswer.setText("");
			this.removeActor(btn_ShowAnswer);
			btn_CheckAnswer.setText("Chơi Lại");
		}
		
		
		Finish = true;
		if(_isWin)
			sound_win.play();
		else
			sound_fail.play();
		Dialog dialog = new Dialog(" ", new Skin(Gdx.files.internal("data/Skin/uiskin.json") ,new TextureAtlas(Gdx.files.internal("data/Skin/uiskin.atlas")))){
			protected void result(Object object) {
				if(_isWin){
					sound_win.stop();
					int max_level = BlockGame.frefs.getInteger(BlockGame.LEVEL_CROSSWORD);
					if(level == (max_level + 1))
						max_level++;
					BlockGame.frefs.putInteger(BlockGame.LEVEL_CROSSWORD, max_level);
					BlockGame.frefs.flush();
				}
				else
					sound_fail.stop();
				if(object.toString().compareTo("back") == 0){
					ScreenManager.inst().game.setScreen(ScreenManager.inst().getMenuLeverScreen());
				}else if( object.toString().compareTo("next") == 0){
					level++;
					ScreenManager.inst().game.setScreen(ScreenManager.inst().getplayScreen(level));
				}
			};
		};
		if(_isWin)
			dialog.button("Back", "back").button("Next", "next");
		else
			dialog.button("OK", "ok").text("Bạn Đã Thua Cuộc");
		dialog.show(stage);
	}
	
}
