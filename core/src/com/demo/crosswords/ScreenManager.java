package com.demo.crosswords;

import com.demo.crosswords.screen.MenuLevelScreen;
import com.demo.crosswords.screen.MenuScreen;
import com.demo.crosswords.screen.SettingScreen;


public class ScreenManager {
	public static ScreenManager _instance;

	public static ScreenManager inst() {
		if (_instance == null) {
			_instance = new ScreenManager();
		}
		return _instance;
	}

	public BlockGame game;

	public void initialize(BlockGame game) {
		this.game = game;
	}

	public ScreenManager() {

	}
	public PlayScreen playScreen;
	public MenuLevelScreen menuLeverScreen;
	public MenuScreen MenuScreen;
	public SettingScreen Setting;

	public PlayScreen getplayScreen(int level) {
		playScreen = new PlayScreen(level);
		return playScreen;
	}
	
	public MenuLevelScreen getMenuLeverScreen() {
		menuLeverScreen = new MenuLevelScreen();
		return menuLeverScreen;
	}
	
	public MenuScreen getMenuScreen(){
		if(MenuScreen == null) {
			MenuScreen = new MenuScreen();
		}
//		MenuScreen = new MenuScreen();
		return MenuScreen;
	}
	
	public SettingScreen getSettingScreen() {

		if(Setting == null) {
			Setting = new SettingScreen();
		}
		return Setting;
	}

}
