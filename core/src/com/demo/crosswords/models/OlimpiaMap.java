package com.demo.crosswords.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

public class OlimpiaMap {
	
	

	public static class Answer{
		public String down[];
		public String across[];
	}
	
	public static class Clue{
		public String across[];
		public String down[];
	}
	
	public static class Size{
		public int cols;
		public int rows;
	}
	
	public static class Groups{
		public int across[][];
		public int down[][];
	}
	
	public String[] Character;
	
	public static OlimpiaMap LoadDB(int level){
		FileHandle file = Gdx.files.internal(String.format("map/olimpia_%d.json", level));
		Json json = new Json();
		OlimpiaMap demo = json.fromJson(OlimpiaMap.class, file);
		return demo;
	}
	
	public String imagetopic;
	public String topic;
	public int down;
	public String grid[];
	public String hint[];
	public OlimpiaMap.Answer answers;
	public OlimpiaMap.Size size;
	
	
}